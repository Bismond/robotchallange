﻿using System;
using System.Collections.Generic;
using HrynchukDmytro.RobotChallenge;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace HrynchukDmytro.RobotChallange.Test
{
    [TestClass]
    public class TestAlgorithmCollect
    {
        [TestMethod]
        public void TestCollectCommand()
        {
            var algorithm = new HrynchukDmytroAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });

            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);
        }
    }
}
