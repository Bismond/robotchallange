﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HrynchukDmytro.RoborChallenge;
using Robot.Common;

namespace HrynchukDmytro.RobotChallenge
{


    public class HrynchukDmytroAlgorithm : IRobotAlgorithm
    {
      

        public HrynchukDmytroAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundCount++;
        }

        public int RoundCount { get; set; }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Position stationPosition = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if ((RoundCount < 35) && (movingRobot.Energy > 400)
                && (190 > DistanceHelper.FindDistance(stationPosition, movingRobot.Position)))
            {
                return new CreateNewRobotCommand();
            }

          


            if (stationPosition == null)
                return null;

            if (stationPosition == movingRobot.Position)
                return new CollectEnergyCommand();
            else
            {

                if (movingRobot.Energy >
                     DistanceHelper.FindDistance(stationPosition, movingRobot.Position))
                {
                    return new MoveCommand() { NewPosition = stationPosition };

                }
                else
                {
                    int x1 = (stationPosition.X + movingRobot.Position.X) / 2;
                    int y1 = (stationPosition.Y + movingRobot.Position.Y) / 2;
                    Position middPosition = new Position(x1, y1);
                    if (movingRobot.Energy > DistanceHelper.FindDistance(middPosition, movingRobot.Position))
                    {
                        return new MoveCommand() { NewPosition = middPosition };
                    }
                    else
                    {
                        int x2 = (middPosition.X + movingRobot.Position.X) / 2;
                        int y2 = (middPosition.Y + movingRobot.Position.Y) / 2;
                        Position midpPosition2 = new Position(x2, y2);
                        if (movingRobot.Energy > DistanceHelper.FindDistance(midpPosition2, movingRobot.Position))
                        {
                            return new MoveCommand() { NewPosition = midpPosition2 };
                        }
                        else
                            x2 = (midpPosition2.X + movingRobot.Position.X) / 2;
                        y2 = (midpPosition2.Y + movingRobot.Position.Y) / 2;
                        Position midpPosition3 = new Position(x2, y2);

                        if (movingRobot.Energy > DistanceHelper.FindDistance(midpPosition2, movingRobot.Position))
                        {
                            return new MoveCommand() { NewPosition = midpPosition3 };
                        }
                        else
                        {
                            x2 = (midpPosition3.X + movingRobot.Position.X) / 2;
                            y2 = (midpPosition3.Y + movingRobot.Position.Y) / 2;
                            Position midpPosition4 = new Position(x2, y2);
                            return new MoveCommand() { NewPosition = midpPosition4 };
                        }
                    }

                }


            }
        }


        public string Author
        {
            get { return "HrynchukDmytro"; }
        }

        public string Description
        {
            get { return "Demoforstudents"; }
        }

        public Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);

                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }

            }

            return nearest == null ? null : nearest.Position;
        }

        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }


        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if ((robot.Position == cell) && (robot.Owner.Name.Equals("HrynchukDmytro")))
                        return false;
                }
            }
            return true;
        }
    }
}


